# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, fields, models, _
import datetime
from pytz import timezone
import pytz
from dateutil.relativedelta import relativedelta
import time
from odoo import tools

class AccountInvoice(models.Model):
    _inherit = 'account.move'


    def _get_date_invoice_tz_33(self):
        res = {}
        dt_format = tools.DEFAULT_SERVER_DATETIME_FORMAT
        ctx = dict(self.env.context)
        tz = ctx.get('tz_invoice_mx', 'America/Mexico_City')
        for invoice in self:

            if invoice.type in ("out_invoice","out_refund"):
                dt_tz = fields.Datetime.context_timestamp(
                    self.with_context(tz=self.env.user.tz),
                    fields.Datetime.from_string(fields.Datetime.now()))

                invoice.date_invoice_tz_33 = dt_tz or False

    invoice_datetime_33 = fields.Datetime(
        'Date Electronic Invoiced ',
        states={'open': [('readonly', True)], 'close': [('readonly', True)]},
        help="Keep empty to use the current date")
    date_invoice_tz_33 = fields.Datetime(
        compute=_get_date_invoice_tz_33, method=True,
        string='Date Invoiced with TZ',
        help='Date of Invoice with Time Zone')


    def copy(self, default=None):
        default = dict(default or {})
        default.update({'invoice_datetime': False, 'date_invoice': False})
        return super(AccountInvoice, self).copy(default)


    def _get_time_zone_33(self):
        res_users_obj = self.env['res.users']
        userstz = res_users_obj.browse(self._uid).partner_id.tz
        a = 0
        if userstz:
            hours = timezone(userstz)
            fmt = '%Y-%m-%d %H:%M:%S %Z%z'
            now = datetime.datetime.now()
            loc_dt = hours.localize(datetime.datetime(
                now.year, now.month, now.day, now.hour, now.minute,
                now.second))
            timezone_loc = (loc_dt.strftime(fmt))
            diff_timezone_original = timezone_loc[-5:-2]
            timezone_original = int(diff_timezone_original)
            s = str(datetime.datetime.now(pytz.timezone(userstz)))
            s = s[-6:-3]
            timezone_present = int(s)*-1
            a = timezone_original + ((
                timezone_present + timezone_original)*-1)
        return a


    def assigned_datetime_33(self, values):

        res = {}
        ctx = dict(self.env.context)
        if values.get(
            'date_invoice', False) and not values.get(
                'invoice_datetime_33', False):


            tz = ctx.get('tz_invoice_mx', 'America/Mexico_City')

            res['invoice_datetime_33'] = fields.Datetime.from_string(fields.Datetime.now()).strftime('%Y-%m-%d %H:%M:%S')
            res['date_invoice'] = values['date_invoice']
        if values.get('invoice_datetime_33', False) and not\
            values.get('date_invoice', False):
            date_invoice = fields.Datetime.context_timestamp(
                self, datetime.datetime.strptime(values['invoice_datetime_33'],
                tools.DEFAULT_SERVER_DATETIME_FORMAT))
            res['date_invoice'] = date_invoice
            res['invoice_datetime_33'] = values['invoice_datetime_33']

        if 'invoice_datetime_33' in values  and 'date_invoice' in values:
            if values['invoice_datetime_33'] and values['date_invoice']:
                date_invoice = datetime.datetime.strptime(
                    values['invoice_datetime_33'],
                    '%Y-%m-%d %H:%M:%S').date().strftime('%Y-%m-%d')
                if date_invoice != values['date_invoice']:
                    date_invoice = fields.Datetime.context_timestamp(
                        self, datetime.datetime.strptime(
                            values['invoice_datetime_33'],
                            tools.DEFAULT_SERVER_DATETIME_FORMAT))
                    res['date_invoice'] = date_invoice
                    res['invoice_datetime_33'] = values['invoice_datetime_33']

        if not values.get('invoice_datetime_33', False) and not values.get(
                'date_invoice', False):
            res['date_invoice'] = fields.Date.context_today(self)
            res['invoice_datetime_33'] = fields.Datetime.now()

        return res


    def action_move_create(self):
        for inv in self.filtered(lambda i: i.type in (
                'out_invoice', 'out_refund')):
            vals_date = self.assigned_datetime_33(
                {'invoice_datetime_33': inv.invoice_datetime_33,
                    'date_invoice': inv.date_invoice})
            inv.write(vals_date)
        return super(AccountInvoice, self).action_move_create()
