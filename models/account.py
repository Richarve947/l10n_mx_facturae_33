# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, fields, models, _


class Sequence(models.Model):
    _inherit = 'ir.sequence'


    serie= fields.Char('Serie de Folios', size=12, required=False)


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    #~ def onchange_company_id(self, cr, uid, ids,company2_id=False, context=None):
        #~ result={}
        #~ list_res=[]
        #~ if company2_id:
            #~ company=self.pool.get('res.company')
            #~ partner=self.pool.get('res.partner')
            #~ address_id=company.browse(cr, uid, company2_id, context=context).partner_id.id
            #~ res_ids=partner.search(cr ,uid, [('type', '=', 'invoice')],context=context)
            #~ if res_ids:
                #~ list_res+=res_ids
            #~ list_res.append(address_id)
            #~ result['domain']={'address_invoice_company_id':[('id','in',list_res)]}
            #~ print result
        #~ return result


    address_invoice_company_id = fields.Many2one('res.partner','Direccion de Factura')
    company2_id = fields.Many2one("res.company", 'Compañia Emisora')
    fiscal = fields.Boolean('Diario SAT')



class AccountInvoice(models.Model):
    _inherit = 'account.move'

    def _get_address_issued_invoice(self):
        a = self.journal_id.address_invoice_company_id and \
            self.journal_id.address_invoice_company_id.id or False
        b = self.journal_id.company2_id and \
            self.journal_id.company2_id.partner_id and \
            self.journal_id.company2_id.partner_id.id or False

        address_invoice = a or b or False
        self.address_issued_id = address_invoice

    def _get_company_emitter_invoice(self):
        for invoice in self:
            company_invoice = invoice.journal_id.company2_id and \
                invoice.journal_id.company2_id.id or invoice.company_id and \
                invoice.company_id.id or False
            invoice.company_emitter_id = company_invoice

    address_issued_id = fields.Many2one(
        compute=_get_address_issued_invoice,
        comodel_name='res.partner', string='Address Issued \
            Invoice', help='This address will be used as address that issued \
            for electronic invoice')
    company_emitter_id = fields.Many2one(
        compute=_get_company_emitter_invoice,
        comodel_name='res.company',
        string='Company Emitter \
            Invoice', help='This company will be used as emitter company in \
            the electronic invoice')

    #~ @api.onchange('journal_id')
    #~ def _onchange_journal_id(self):
        #~ super(AccountInvoice, self)._onchange_journal_id()
        #~ if self.journal_id and self.journal_id.address_invoice_company_id:
            #~ self.address_invoice_company_id = self.journal_id.address_invoice_company_id.id
        #~ if self.journal_id and self.journal_id.company2_id:
            #~ self.company2_id = self.journal_id.company2_id.id


class AccountTax(models.Model):
    _inherit = 'account.tax'

    code_sat = fields.Selection([('001','ISR-001'), ('002','IVA-002'),('003','IEPS-003')], 'Codigo SAT')
