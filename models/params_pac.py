# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Copyright (c) 2013
#    All Rights Reserved.
############################################################################
#    Coded by: rodolfo.lopez.t@gmail.com
############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import fields, models


class ParamsPac(models.Model):
    _name = 'params.pac'

    def _get_method_type_selection(self):
        # From module of PAC inherit this function and add new methods
        types = []
        return types

    name = fields.Char('Nombre', size=128)
    url_webservice = fields.Char('URL WebService', size=256)
    namespace = fields.Char(
        'NameSpace', size=256)
    user = fields.Char('Usuario', size=128)
    password = fields.Char(
        'Contraseña', size=128)
    method_type = fields.Selection([('firmar', 'Timbrar'),
                                    ('cancelar', 'Cancelar')],
                                    string='Proceso')
    company_id = fields.Many2one(
        'res.company', 'Companñia', default=lambda self: self.env.user.company_id)
    active = fields.Boolean(
        'Activo', help='Indicate if this param is active', default=True)
    sequence = fields.Integer(
        'Sequencia', default=10)
    certificate_link = fields.Char(
        'Certificate link', size=256,
        help='PAC have a public certificate that is necessary by customers to check the validity of the XML and PDF')
    # 'link_type': fields.selection([('production','Produccion'),
    # 'test','Pruebas')],"Tipo de ligas"),
    user_id = fields.Many2one(
        'res.users', 'User Params Pac',
        help="The user responsible for this params pac")
    #~ 'res_pac': fields.many2one(
        #~ 'res.pac', 'Res Pac',
        #~ help="The res pac configuration for this params pac"),
    #~ 'token': fields.char(
        #~ 'Token', size=128, help='this field is required by some pacs'),
