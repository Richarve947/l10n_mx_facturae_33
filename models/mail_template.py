# -*- encoding: utf-8 -*-
# © <2018> <Quadit, S.A. de C.V.>
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import base64
import logging

from odoo import _, api, fields, models, tools
from odoo.exceptions import UserError

class MailTemplate(models.Model):
    _inherit = "mail.template"

    @api.multi
    def generate_email(self, res_ids, fields=None):
        payment_obj = self.env['account.invoice']
        attach_xml_ids = []
        attach_pdf_ids = []
        values = super(MailTemplate, self).generate_email(res_ids, fields=None)
        if self.env.context.get('active_id'):
            if values.get(self.env.context.get('active_id')):
                if self.env.context.get('active_model') == 'account.invoice':
                    values[self.env.context.get('active_id')]['attachments'] = []
                    payment_row = payment_obj.browse(self.env.context.get('active_id'))
                    fname = payment_row.company_id.partner_id.vat[2:] + '_' + (payment_row.number or '')
                    attach_rows = self.env['ir.attachment'].search([('res_id','=',self.env.context.get('active_id')), ('res_model','=','account.invoice'),('datas_fname','like',fname)])
                    for atta in attach_rows:
                        ext = atta.datas_fname.split('.')[-1].lower()
                        if 'xml' == ext:
                            attach_xml_ids.append(atta.id)
                        elif 'pdf' == ext:
                            attach_xml_ids.append(atta.id)
                    if attach_xml_ids:
                        values[self.env.context.get('active_id')]['attachment_ids'].extend(attach_xml_ids)
            elif values.get('res_id') == self.env.context.get('active_id') and values.get('model') == 'account.invoice':
                values.update({'attachments':[]})
                payment_row = payment_obj.browse(self.env.context.get('active_id'))
                fname = payment_row.company_id.partner_id.vat[2:] + '_' + (payment_row.number or "")
                attach_rows = self.env['ir.attachment'].search([('res_id','=',self.env.context.get('active_id')), ('res_model','=','account.invoice'),('datas_fname','like',fname)])
                for atta in attach_rows:
                    ext = atta.datas_fname.split('.')[-1].lower()
                    if 'xml' == ext:
                        attach_xml_ids.append(atta.id)
                    elif 'pdf' == ext:
                        attach_xml_ids.append(atta.id)
                if attach_xml_ids:
                    values.update({'attachment_ids':attach_xml_ids})
        return values
